FROM revolutionsystems/python:3.6.9-wee-optimized-lto
ENV PYTHONUNBUFFERED 1

RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
    && echo "deb http://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
    && curl -sL https://deb.nodesource.com/setup_8.x | bash - \
    && apt-get update && apt-get install build-essential mysql-client default-libmysqlclient-dev yarn -y \
    && pip install --upgrade pip \
    && mkdir /code

WORKDIR /code
ADD . /code
COPY ./bashrc_example /root/.bashrc

RUN npm install -g @vue/cli \
    && cat /code/.gitconfig > /root/.gitconfig \
    && pip install pip-tools \ 
    && pip-sync /code/requirements-dev.txt
