module.exports = {
    title: 'Heimlich',
    description: 'Ticketing system built with Django + Vue.js',
    themeConfig: {
        sidebar: 'auto',
        nav: [
            { text: 'Home', link: '/' },
            { text: 'Developing', link: '/developing/' },
            { text: 'Project Structure', link: '/project_structure.html' },
            { text: 'Code', link: 'https://gitlab.com/uninen/heimlich' },
        ]
    }
}