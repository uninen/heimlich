---
home: true
actionText: Developing →
actionLink: /developing/
features:
- title: Objectives
  details: Most projects need ticketing or task lists. Heimlich is designed to be a simple drop-in solution for simple needs.
- title: Use Cases
  details: Simple TODO-list, simple ticketing, multi-project ticketing, kanban ticketing, kanban TODO-list.
- title: Pre Alpha!
  details: This project is still in pre-alpha status.
footer: MIT Licensed | Copyright © 2018-present Ville Säävuori
---

## Open Questions

* Should column states be explicitly mapped to ticket states?
* How do we implement "smart boards" (ie. tickets with status x or label y)

## Inspiration

* Interface inspiration: https://scrumpy.io/features
* More inspiration: https://tomato5.io/
* https://github.blog/2019-10-14-introducing-autolink-references/