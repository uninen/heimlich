# Development

## Working With VS Code and Docker (the recommended way)

This project has been set up with configuration for VS Code that allows you to set up your 
development environment with Docker without any manual installing or tweaking. This is the 
preferred way to develop and only one you should be asking for any support :)

Make sure you have the following installed:

* Docker
* VS Code
* VS Code remote extension for Docker

### Setting Up VS Code

Open VS Code (Hint: You probably want to install terminal command `code`) and when it asks you to 
open the project in Dev Container, do so. That's it. For the first time you open the project, the
process will take few minutes as Docker will first download and install needed images, the project
Docker image gets built and then all the necessary Python and JavaScript requirements get installed.

## Setting Up Manually

To get started with development, you need to have following installed:

* [Python 3.6+](https://www.python.org/)
    * [Virtualenv](https://virtualenv.pypa.io/en/latest/)
    * [pip](https://pip.pypa.io/en/stable/)
    * [pip-tools](https://github.com/jazzband/pip-tools/)
* [NodeJS 8+](https://nodejs.org/en/)
* [Vue CLI 3+](https://cli.vuejs.org/)
* [Yarn](https://yarnpkg.com/en/)

### Setting Up Python Environment

1. Create a virtualenv for the project. ([Virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/) is your friend!)
1. Export necessary environment variables. There are examples in `.env.example`. (You can copy 
`.env.example` as `.env` and then add `export $(cat /my/project/dir/.env` to projects virtualenv 
postactivate script to automatically export these every time the virtualenv is activated.))
1. Activate the virtual environment and then install `pip-tools` in it: `pip install pip-tools`
1. Install development requirements: `pip-sync requirements-dev.txt`

You're now ready to run Django development server:

`python manage.py runserver 0.0.0.0:8000`

### Setting Up JavaScript Environment

Make sure you have up to date NodeJS, Yarn and Vue cli installed.

1. Install JavaScript dependencies: `yarn`

Now you can start Vue CLI UI (but make sure it doesn't try to hijack port 8000): `vue ui -p 8181`.

## Testing

Python tests are run with pytest: `pytest`.

Integration tests are run with [Cypress](https://www.cypress.io/) via yarn: (note that in 
development environment Django runserver must be running for e2e tests to work) `yarn test:e2e`.

## Coding Guidelines

For the backend, follow
[Django design philosophies][1] and
[coding style][2] along with general PEP 8 rules, with the notable exception of line length which is
120 characters. Make sure your editor has Black, Pylint and Flake8 installed for automated ¨
formatting and linting.

For the frontend, use [Airbnb JavaScript Style Guide](https://github.com/airbnb/javascript) with
the exception of having **no semicolons** (as we use tools to worry about them for us).
[Vue Style Guide](https://vuejs.org/v2/style-guide/) also has some pointers on how to format Vue 
code. Make sure your editor understand Eslint rules defined in the repo.

**Commmit messages** should follow [Django commit message guidelines][3], and be in the past tense
(For example: "Added feature for creating awesomeness"). If the message is long, use a short subject
line followed by one empty line and then a longer body.

[1]: https://docs.djangoproject.com/en/2.1/misc/design-philosophies/
[2]: https://docs.djangoproject.com/en/2.1/internals/contributing/writing-code/coding-style/
[3]: https://docs.djangoproject.com/en/2.1/internals/contributing/committing-code/#committing-guidelines

----

Note: This documentation is written for MacOS. If your experience with other platform differs, feel
free to open a merge request.
