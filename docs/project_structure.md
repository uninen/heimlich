# Project Structure

This project consists of three main parts:

1. Django backend (requirements in root, code under `/heimlich`)
1. Vue frontend (under `/vue`)
1. Documentation (under `/docs`)

## Backend

Backend application is built with [Django](https://www.djangoproject.com/) and Python 3.6.

## Frontend

Frontend is a [vue-cli](https://cli.vuejs.org/) application that uses Vue 2.6.

## Documentation

The documentation is built with [VuePress](https://vuepress.vuejs.org) in mind so we get a 
functional Web site basically for free.