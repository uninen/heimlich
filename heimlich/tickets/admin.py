from django.contrib import admin

from heimlich.tickets.models import Ticket


class TicketAdmin(admin.ModelAdmin):
    pass


admin.site.register(Ticket, TicketAdmin)
