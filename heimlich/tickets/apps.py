from django.apps import AppConfig


class TicketsConfig(AppConfig):
    name = "heimlich.tickets"
    label = "heimlich_tickets"
    verbose_name = "Tickets"
