from django.core.management.base import BaseCommand, CommandError
from django.conf import settings

from heimlich.utils.fixture_runner import run_fixture


class Command(BaseCommand):
    help = "Allows interacting with database from e2e test scripts."

    def add_arguments(self, parser):
        parser.add_argument("fixture_name", nargs="+", type=str)

    def handle(self, *args, **options):
        if not settings.DEBUG:
            raise CommandError("This command can only be run when DEBUG=True")

        for fixture_name in options["fixture_name"]:
            if run_fixture(fixture_name):
                self.stdout.write(
                    self.style.SUCCESS(f"Succesfully ran fixture {fixture_name}.")
                )
            else:
                raise CommandError(f"Fixture {fixture_name} not found.")
