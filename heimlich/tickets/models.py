from django.db import models
from django.contrib.auth.models import User

from ordered_model.models import OrderedModel


ISSUE_STATE_CHOICES = (("Open", "open"), ("Closed", "closed"))


class Project(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

    class Meta:
        app_label = "heimlich_tickets"


class KanbanBoard(models.Model):
    name = models.CharField(max_length=50)
    project = models.ForeignKey(
        Project,
        related_name="kanban_boards",
        on_delete=models.SET_NULL,
        null=True,
        default=None,
    )

    def __str__(self):
        return self.name

    class Meta:
        app_label = "heimlich_tickets"


class BoardColumn(models.Model):
    board = models.ForeignKey(
        KanbanBoard, related_name="columns", on_delete=models.CASCADE
    )
    name = models.CharField(max_length=50)

    ordering = models.PositiveSmallIntegerField(default=0)
    tickets = models.ManyToManyField(
        "Ticket", blank=True, related_name="boards_on"
    )  # One ticket can be on multiple boards

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["-pk", "ordering"]
        app_label = "heimlich_tickets"


class Label(models.Model):
    name = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        app_label = "heimlich_tickets"


class Ticket(OrderedModel):
    reporter = models.ForeignKey(
        User,
        related_name="reported_tickets",
        on_delete=models.SET_NULL,
        null=True,
        default=None,
    )
    project = models.ForeignKey(
        Project,
        related_name="tickets",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        default=None,
    )
    title = models.CharField(max_length=100)
    text = models.TextField(blank=True, default="")
    state = models.CharField(
        max_length=10,
        choices=ISSUE_STATE_CHOICES,
        default=ISSUE_STATE_CHOICES[0][0],
        db_index=True,
    )

    labels = models.ManyToManyField(
        Label, blank=True, related_name="tickets", related_query_name="ticket"
    )
    upvoters = models.ManyToManyField(User, blank=True, related_name="tickets_upvoted")
    assignee = models.ForeignKey(
        User,
        related_name="assigned_tickets",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        default=None,
    )

    gitlab_id = models.CharField(
        max_length=20, unique=True, null=True, blank=True, default=None, db_index=True
    )

    created_at = models.DateTimeField(auto_now_add=True)
    last_modified_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"#{self.pk} {self.title}"

    class Meta:
        app_label = "heimlich_tickets"
        ordering = ["order", "created_at"]
