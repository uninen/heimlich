from django.http import HttpResponseBadRequest

from rest_framework import serializers, viewsets
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework import permissions

from heimlich.tickets.models import Ticket


class TicketSerializer(serializers.ModelSerializer):
    labels = serializers.StringRelatedField(many=True)

    class Meta:
        model = Ticket
        fields = "__all__"


class TicketViewSet(viewsets.ModelViewSet):
    queryset = Ticket.objects.all()
    serializer_class = TicketSerializer
    permission_classes = [permissions.IsAuthenticated]


@api_view(["POST"])
@permission_classes([permissions.IsAuthenticated])
def move_ticket_to(request):
    """
    Moves a ticket into given position related to all other tickets.

    Takes two arguments:

    'pk': Ticket pk
    'to': New position in ticket list.
    """
    ticket_pk = request.data.get("pk", None)
    move_to = request.data.get("to", None)

    if ticket_pk is not None and move_to is not None:
        ticket = Ticket.objects.get(pk=ticket_pk)
        ticket.to(int(move_to))
        return Response("OK")
    else:
        return HttpResponseBadRequest("Missing pk 'or' 'to'.")
