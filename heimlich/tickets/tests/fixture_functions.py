from random import randint, sample, seed

from django.contrib.auth.models import User
from django.db.utils import IntegrityError

from heimlich.tickets.models import Ticket, Label
from model_mommy.recipe import Recipe
from faker import Factory
from faker.providers import lorem

fake = Factory.create()
fake.add_provider(lorem)

PASSWORD = "8RP7,^g8ea;%86f(sV"
EMAIL = "testuser@example.dlt"
LABELS = [
    "bug",
    "frontend",
    "backend",
    "ui",
    "security",
    "refactoring",
    "task",
    "idea",
    "todo",
    "api",
    "util",
]

seed()


def generate_title():
    return fake.sentence(nb_words=6, variable_nb_words=True, ext_word_list=None)[:-1]


def generate_label_list():
    num_labels = randint(1, 4)
    return sample(LABELS, num_labels)


def get_random_label():
    return LABELS[randint(0, len(LABELS))]


def get_labels():
    label_list = []
    for label in generate_label_list():
        obj, created = Label.objects.get_or_create(name=label)
        label_list.append(obj)
    return label_list


label_recipe = Recipe(Label, name=get_random_label)
ticket_recipe = Recipe(Ticket, title=generate_title)


def create_test_user():
    try:
        test_user = User.objects.create_user(
            username="testuser", email=EMAIL, password=PASSWORD
        )
    except IntegrityError:
        print("Test user already existed.")
        test_user = User.objects.get(username="testuser", email=EMAIL)
    return test_user


def create_tickets(num=10):
    tickets = ticket_recipe.make(_quantity=num)
    for ticket in tickets:
        for label in get_labels():
            ticket.labels.add(label)
    return tickets


def create_labels():
    for label in LABELS:
        try:
            Label.objects.create(name=label)
        except Exception:
            pass


def reset_tickets():
    Ticket.objects.all().delete()
    Label.objects.all().delete()
