# -*- coding: utf-8 -*-
import pytest

from heimlich.tickets.models import Ticket, Label
from heimlich.tickets.views import empty_index, fixture_runner
from heimlich.tickets.utils.fixture_runner import run_fixture


@pytest.mark.django_db
def test_empty_homepage(rf):
    request = rf.get("/")
    response = empty_index(request)
    assert response.status_code == 200, "Empty index should work"


def test_fixture_runner_view(rf):
    request = rf.get("/fixture_runner/")
    response = fixture_runner(request)
    assert response.status_code == 200, "Fixture runner should return 200"


@pytest.mark.django_db
def test_run_fixture():
    assert run_fixture("notvalid") is False
    assert run_fixture("create_test_user") is True

    assert Ticket.objects.all().count() == 0
    assert run_fixture("create_tickets") is True
    assert Ticket.objects.all().count() == 5

    assert run_fixture("reset_tickets") is True
    assert Ticket.objects.all().count() == 0

    assert Label.objects.all().count() == 0
    assert run_fixture("create_labels") is True
    assert Label.objects.all().count() == 11
