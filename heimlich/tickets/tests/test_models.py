# -*- coding: utf-8 -*-
import pytest
from model_mommy import mommy

from heimlich.tickets.models import Project, KanbanBoard, BoardColumn, Label

pytestmark = pytest.mark.tickets


@pytest.mark.django_db
def test_models():
    mommy.make("heimlich_tickets.Label")
    label = Label.objects.get(pk=1)
    assert Label.objects.all().count() == 1, "Label creation should work"
    assert label.name == str(label)

    mommy.make("heimlich_tickets.Project")
    project = Project.objects.get(pk=1)
    assert Project.objects.all().count() == 1, "Project creation should work"
    assert project.name == str(project)

    mommy.make("heimlich_tickets.KanbanBoard")
    board = KanbanBoard.objects.get(pk=1)
    assert KanbanBoard.objects.all().count() == 1, "KanbanBoard creation should work"
    assert board.name == str(board)

    mommy.make("heimlich_tickets.BoardColumn")
    boardcolumn = BoardColumn.objects.get(pk=1)
    assert BoardColumn.objects.all().count() == 1, "BoardColumn creation should work"
    assert boardcolumn.name == str(boardcolumn)
