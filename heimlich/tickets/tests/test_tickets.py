# -*- coding: utf-8 -*-
import pytest
from model_mommy import mommy
from rest_framework.test import APIRequestFactory, force_authenticate

from heimlich.tickets.models import Ticket
from heimlich.tickets.tests.fixture_functions import create_test_user
from heimlich.tickets.serializers import TicketViewSet, move_ticket_to

pytestmark = pytest.mark.tickets


@pytest.mark.django_db
def test_ticket_creation():
    factory = APIRequestFactory()
    test_user = create_test_user()

    mommy.make("heimlich_tickets.Ticket")
    assert Ticket.objects.all().count() == 1, "Anonymous ticket creation should work"

    mommy.make("heimlich_tickets.Ticket", reporter=test_user)
    assert Ticket.objects.all().count() == 2, "Ticket creation w/ reporter should work"

    request = factory.get("/api/tickets/")
    force_authenticate(request, user=test_user)
    view = TicketViewSet.as_view({"get": "list"})
    response = view(request)
    assert response.status_code == 200, "Listing tickets via API endpoint should work"

    ticket = Ticket.objects.get(pk=1)
    assert "#1" in str(ticket)

    request = factory.post("/api/tickets/move_to/", {"pk": 1, "to": 2})
    force_authenticate(request, user=test_user)
    response = move_ticket_to(request)
    assert response.status_code == 200, "Reordering tickets should work"

    request = factory.post("/api/tickets/move_to/", {"pk": 1})
    force_authenticate(request, user=test_user)
    response = move_ticket_to(request)
    assert (
        response.status_code == 400
    ), "Reordering tickets should not work with bad input"
