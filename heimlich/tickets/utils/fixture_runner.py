from heimlich.tickets.tests.fixture_functions import (
    create_test_user,
    create_tickets,
    create_labels,
    reset_tickets,
)


def run_fixture(fixture):
    if fixture == "create_test_user":
        create_test_user()
        return True
    elif fixture == "create_tickets":
        create_tickets(5)
        return True
    elif fixture == "create_labels":
        create_labels()
        return True
    elif fixture == "reset_tickets":
        reset_tickets()
        return True
    else:
        return False
