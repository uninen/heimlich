from django.http import HttpResponse

from heimlich.tickets.utils.fixture_runner import run_fixture


def empty_index(request):
    return HttpResponse("OK")


def fixture_runner(request):
    run_fixture(request.GET.get("f"))
    return HttpResponse("OK")
