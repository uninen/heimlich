import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Login from './views/Login.vue'
import Logout from './views/Logout.vue'
import TodoList from './views/TodoList.vue'
import store from './store'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      component: () =>
        import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta: {
        title: 'Log In'
      }
    },
    {
      path: '/logout',
      name: 'logout',
      component: Logout,
      meta: {
        title: 'Log Out'
      }
    },
    {
      path: '/todos',
      name: 'todos',
      component: TodoList,
      meta: {
        title: 'ToDo List'
      }
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.path !== '/login' && to.path !== '/logout' && !store.state.authToken) {
    next('/login')
  } else {
    next()
  }
})

export default router
