import Vue from 'vue'
import Vuex from 'vuex'

import findIndex from 'lodash-es/findIndex'
import filter from 'lodash-es/filter'

import { BASE_URL } from '@/conf.js'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    authToken: null,
    username: '',
    isAuthenticated: false,
    isInited: false,
    tickets: []
  },

  getters: {
    tickets: state => {
      return filter(state.tickets, function (obj) {
        return obj.state === 'Open'
      })
    },
    todoItems: state => {
      return state.tickets
    }
  },

  mutations: {
    setStateAttr(state, payload) {
      // Helper function to reduce setFoo(state, value) => state.foo = value for every 'foo'
      state[payload.attr] = payload.value
    },

    resetState(state) {
      state.authToken = null
      state.username = ''
      state.isAuthenticated = false
      state.isInited = false
      state.tickets = []
    },

    addTicket(state, payload) {
      state.tickets.unshift(payload)
    },

    removeTicket(state, pk) {
      let index = findIndex(state.tickets, { 'id': pk })
      if (index !== -1) {
        state.tickets.splice(index, 1)
      }
    },

    updateTickets(state, payload) {
      state.tickets = payload
    }
  },

  actions: {
    login({ commit }, payload) {
      commit('setStateAttr', {
        attr: 'authToken',
        value: payload.token
      })
      commit('setStateAttr', {
        attr: 'username',
        value: payload.username
      })
      commit('setStateAttr', {
        attr: 'isAuthenticated',
        value: true
      })
    },

    logout({ commit }) {
      commit('resetState')
    },

    initApp({ commit }) {
      this._vm.$http
        .get(BASE_URL + '/api/tickets.json')
        .then(response => {
          commit('setStateAttr', {
            attr: 'tickets',
            value: response.data.results
          })
          commit('setStateAttr', {
            attr: 'isInited',
            value: true
          })
        })
    }
  }
})
