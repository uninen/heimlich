// https://docs.cypress.io/api/introduction/api.html

describe('Test app login', () => {
  before(function () {
    cy.request(Cypress.env('API_URL') + '/fixture_runner/?f=create_test_user')
  })

  it('Visits the app root url anonymously, should see the login window', () => {
    cy.visit('/')
    cy.location('pathname').should('eq', '/login')
    cy.contains('h1', 'Log In')
  })

  it('Submits login form with bad credentials', () => {
    cy.visit('/')
      .get('#username').type('testuser')
      .get('#password').type('testpassword')
      .get('#submit-btn').click()
    cy.contains('div', 'Error: username or password is incorrect.')
    cy.get('li.dropdown').should('not.exist')
  })

  it('Submits login form with good credentials', () => {
    cy.visit('/login')
      .get('#username').type('testuser')
      .get('#password').type('8RP7,^g8ea;%86f(sV')
      .get('#submit-btn').click()
      .wait(500)
      .visit('/')
    cy.contains('h1', 'Heimlich')
    cy.get('li.dropdown').should('exist')
    cy.contains('em', 'testuser')
  })

  it('Logs in and logs out', () => {
    cy.visit('/login')
      .get('#username').type('testuser')
      .get('#password').type('8RP7,^g8ea;%86f(sV')
      .get('#submit-btn').click()
      .wait(500)
      .visit('/logout')
    cy.get('#submit-btn').click()
      .wait(500)
      .visit('/')
    cy.location('pathname').should('eq', '/login')
    cy.contains('h1', 'Log In')
  })
})
