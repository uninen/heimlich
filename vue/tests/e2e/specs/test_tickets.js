describe('Test tickets', () => {
  before(function() {
    cy.request(Cypress.env('API_URL') + '/fixture_runner/?f=create_test_user')
      .request(Cypress.env('API_URL') + '/fixture_runner/?f=reset_tickets')
      .request(Cypress.env('API_URL') + '/fixture_runner/?f=create_tickets')
    cy.visit('/login')
      .get('#username').type('testuser')
      .get('#password').type('8RP7,^g8ea;%86f(sV')
      .get('#submit-btn').click()
    cy.get('#home-h', { timeout: 10000 })
  })

  it('Tests ticket list', () => {
    cy.visit('/')
    cy.get('.ticket-list-item').its('length').should('eq', 5)
  })

  it('Tests ticket creation', () => {
    cy.get('#create-input').type('test ticket')
      .get('#add-btn').click()
      .wait(500)
    cy.get('.ticket-list-item').its('length').should('eq', 6)
  })

  it('Tests ticket deletion', () => {
    cy.get(':nth-child(1) > .row > .col-2 > form > .remove-button').click()
      .wait(500)
    cy.get('.ticket-list-item').its('length').should('eq', 5)
  })
})
