describe('Test tickets', () => {
  before(function() {
    cy.request(Cypress.env('API_URL') + '/fixture_runner/?f=create_test_user')
      .request(Cypress.env('API_URL') + '/fixture_runner/?f=reset_tickets')
      .request(Cypress.env('API_URL') + '/fixture_runner/?f=create_tickets')
    cy.visit('/login')
      .get('#username').type('testuser')
      .get('#password').type('8RP7,^g8ea;%86f(sV')
      .get('#submit-btn').click()
    cy.get('#home-h', { timeout: 10000 })
  })

  it('Tests todo page', () => {
    cy.get('#nav-todo-link').click()
    cy.get('.ticket-list-item').its('length').should('eq', 5)
    cy.get('.ticket-list').should('not.contain', '#')
  })
})
