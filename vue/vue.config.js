const path = require('path')

module.exports = {
  lintOnSave: false,
  configureWebpack: {
    module: {
      rules: [
        {
          include: path.resolve('node_modules/bootstrap-vue'),
          sideEffects: false
        }
      ]
    }
  }
}
